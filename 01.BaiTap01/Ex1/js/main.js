"use strict";

function initMap() {
	var map;
	document.getElementById("button").addEventListener("click", displayMap);

	function displayMap() {
		var latitude = document.getElementById("latitude");
		var longitude = document.getElementById("longitude");
		var address = document.getElementById("address");

		var condition = latitude.value == "" ||
			longitude.value == "" ||
			latitude.value.indexOf(" ") != -1 ||
			longitude.value.indexOf(" ") != -1 ||
			isNaN(latitude.value) ||
			isNaN(longitude.value);

		if (condition) {
			alert("Dữ liệu không hợp lệ!");
			latitude.value = "";
			longitude.value = "";
		} else {
			// alert("Success");
			latitude = parseFloat(latitude.value);
			longitude = parseFloat(longitude.value);

			map = new google.maps.Map(document.getElementById('map'), {
				center: { lat: latitude, lng: longitude },
				zoom: 15
			});

			var geocoder = new google.maps.Geocoder;
			var infowindow = new google.maps.InfoWindow;

			geocoder.geocode({ 'location': { lat: latitude, lng: longitude } }, function(results, status) {
				if (status === 'OK') {
					if (results[0]) {
						var marker = new google.maps.Marker({
							position: { lat: latitude, lng: longitude },
							map: map
						});
						console.log(results[0].formatted_address);
						address.children[0].innerHTML = results[0].formatted_address;

						infowindow.setContent(results[0].formatted_address);
						infowindow.open(map, marker);
					} else {
						window.alert('No results found');
					}
				} else {
					window.alert('Geocoder failed due to: ' + status);
				}
			});
		}

	}
}