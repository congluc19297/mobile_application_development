var express = require('express')
  , router = express.Router();
  
router.use('/ex2API', require('./ex2API'));
  
module.exports = router;