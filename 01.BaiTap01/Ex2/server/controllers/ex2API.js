var express = require('express')
  , router = express.Router();

router.get('/',function(req,res){
	if(!req.query) {
		return res.json({
			message: 'Data missing',
			error: true
		});
	}
	var R = 6371e3; // metres

	var la1 = req.query.pointA.latitude,
		la2 = req.query.pointB.latitude,

		lo1 = req.query.pointA.longitude,
		lo2 = req.query.pointB.longitude;

	var dLat = (la2 - la1) * (Math.PI / 180),
		dLon = (lo2 - lo1) * (Math.PI / 180),
		la1ToRad = la1 * (Math.PI / 180),
		la2ToRad = la2 * (Math.PI / 180),
		a 	= Math.sin(dLat / 2) * Math.sin(dLat / 2) + Math.cos(la1ToRad)
				* Math.cos(la2ToRad) * Math.sin(dLon / 2) * Math.sin(dLon / 2),
		c 	= 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a)),
		d 	= R * c ;
		d 	= d * Math.PI / 180;

   	return res.json({
		message: 'Success',
		data: {
			points : req.query,
			distance : d
		},
		error: false
	});
});




module.exports = router;