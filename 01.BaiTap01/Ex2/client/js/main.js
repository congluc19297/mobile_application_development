"use strict";
$(document).ready(function() {
	function checkNaN(value) {
		if(value == "" || value.indexOf(" ") != -1 || isNaN(value)) {
			return true;
		}
	}

	$("#button").click(function() {
		var latitudeA = document.getElementById("latitude_1");
		var longitudeA = document.getElementById("longitude_1");

		var latitudeB = document.getElementById("latitude_2");
		var longitudeB = document.getElementById("longitude_2");

		var condition = checkNaN(latitudeA.value) || checkNaN(longitudeA.value)
					|| checkNaN(latitudeB.value) || checkNaN(longitudeB.value);

		if( condition ) {
			alert("Dữ liệu không hợp lệ!");
			latitudeA.value = "";
			longitudeA.value = "";
			latitudeB.value = "";
			longitudeB.value = "";
		} else {
			if( Math.abs(latitudeA.value) <= 90 && Math.abs(latitudeB.value) <= 90 &&
				Math.abs(longitudeA.value) <= 180 && Math.abs(longitudeB.value) <= 180 ) {
				$.ajax({
					url		: 'http://localhost:3000/ex2API/',
					type	: 'get',
					data	: { 
						pointA : { latitude: latitudeA.value, longitude: longitudeA.value },
						pointB : {latitude: latitudeB.value, longitude: longitudeB.value }
					},
					//timeout	: 3000,							// 1s
					success	: function(data, status){
						console.log(data);
						$("#distance h2").html("Khoảng cách giữa hai điểm theo bề mặt cầu là: " + data.data.distance + "m");
					}
				})
			} else {
				alert("|Latitude| <= 90 và |Longitude| <= 180");
			}	
		}
	});
});