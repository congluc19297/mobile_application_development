package com.example.trancongluc.chuyendoitigia;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

//    EditText edtHoTen;
//    Button btnClick;
    //EditText edtHoTen;
    //Button btnClick;
    EditText edtDollars;
    EditText edtEuros;
    EditText edtVND;

    Button btnClear;
    Button btnConvert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtDollars = (EditText) findViewById(R.id.txtUSDollars);
        edtEuros = (EditText) findViewById(R.id.txtEuros);
        edtVND = (EditText) findViewById(R.id.txtVietNam);

        btnClear = (Button) findViewById(R.id.btnClear);
        btnConvert = (Button) findViewById(R.id.btnConvert);

        btnClear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edtDollars.setText("");
                edtEuros.setText("");
                edtVND.setText("");
            }
        });
        //1 dollars = 22 727.2727 VND;
        //1 U.S. dollar 0.812180103 Euros
        btnConvert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!edtDollars.getText().toString().trim().isEmpty()) {
                    //Log.e(edtDollars.getText().toString().trim() + "luc", "Dollars");
                    double dollars = Double.parseDouble(edtDollars.getText().toString());

                    String toVND = String.valueOf(dollars * 22727.2727);
                    String toEuro = String.valueOf(dollars * 0.812180103);

                    edtEuros.setText(toEuro);
                    edtVND.setText(toVND);
                } else if (!edtEuros.getText().toString().trim().isEmpty()) {

                    //Log.e(edtEuros.getText().toString().trim() + "luc", "edtEuros");
                    double euros = Double.parseDouble(edtEuros.getText().toString());

                    String toVND = String.valueOf((euros / 0.812180103) * 22727.2727);
                    String toDollar = String.valueOf(euros / 0.812180103);

                    edtVND.setText(toVND);
                    edtDollars.setText(toDollar);


                } else if (!edtVND.getText().toString().trim().isEmpty()) {
                    //Log.e(edtVND.getText().toString().trim() + "luc", "edtVND");
                    double vnds = Double.parseDouble(edtVND.getText().toString());

                    String toDollar = String.valueOf(vnds / 22727.2727);
                    String toEuro = String.valueOf((vnds / 22727.2727) * 0.812180103);

                    edtEuros.setText(toEuro);
                    edtDollars.setText(toDollar);
                } else {
                    Toast.makeText(MainActivity.this, "Chưa nhập dữ liệu!", Toast.LENGTH_LONG).show();

                }

            }
        });

    }
}
